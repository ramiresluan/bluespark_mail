unit BlueSpark;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxGDIPlusClasses, Vcl.ExtCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet,
  Vcl.StdCtrls, IdMessage, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase,
  IdSMTP, FireDAC.Moni.Base, FireDAC.Moni.RemoteClient, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL, WinInet, idftp,WinSock, IdAttachmentFile,System.Threading, System.DateUtils, ACBrMail,
  System.StrUtils, Vcl.FileCtrl, acPNG, acImage, System.IOUtils;

type
  TfPrincipal = class(TForm)
    FDConGreatOcean: TFDConnection;
    TVerificarEmail: TTimer;
    fEnvios: TFDQuery;
    fEnviosENVIOID: TLargeintField;
    fEnviosDATA: TDateField;
    fEnviosHORA: TTimeField;
    fEnviosUSUARIO: TIntegerField;
    fEnviosCORPOEMAIL: TWideMemoField;
    fEnviosENVIADOS: TStringField;
    fEnviosASSUNTO: TWideStringField;
    fEnviosEMAILUSU: TWideStringField;
    fEnviosCLIENTEID: TIntegerField;
    fEnviosPERSON: TWideStringField;
    fEnviosEMAILS: TWideStringField;
    fEnviosCATEGORIA: TWideStringField;
    IdFTP: TIdFTP;
    fEnviosANEXOS: TWideMemoField;
    fEnviosHORAENVIO: TTimeField;
    lbThread: TLabel;
    TrayIcon: TTrayIcon;
    Panel2: TPanel;
    Panel1: TPanel;
    Image1: TImage;
    Panel3: TPanel;
    Button1: TButton;
    lbStatus: TLabel;
    imMinimizar: TsImage;
    imClose: TsImage;
    procedure TVerificarEmailTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    function FTPDirExistsTry(pDiretorio: String; pIdFTP: TIdFTP): Boolean;
    procedure SaveLog(pMsg:String);

    procedure ThreadListFilesFTP(pPasta:String);
    procedure OnTerminateThreadListFilesFTP(Sender: TObject);

    procedure ThreadSendEmail;
    procedure OnTerminateThreadSendEmail(Sender: TObject);

    function EnviarEmailAcbr(pHtml: AnsiString; pEmail, pAssunto ,pArquivo,pPerson: String; pVersion:Integer): Boolean;
    procedure imCloseClick(Sender: TObject);
    procedure imMinimizarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TrayIconClick(Sender: TObject);
    procedure Panel1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPrincipal: TfPrincipal;
  vListaArquivos: TStringList;
  vCountListaArquivos: Integer;
  vThreadListFilesFTP: TThread;
  vThreadSendEmail: TThread;

implementation

  uses
  System.Types,
  IBX.IBQuery, Winapi.ShellAPI,UGeralPaf;

{$R *.dfm}

procedure TfPrincipal.Button1Click(Sender: TObject);
begin
  ThreadSendEmail;
end;

function TfPrincipal.EnviarEmailAcbr(pHtml: AnsiString; pEmail, pAssunto ,pArquivo,pPerson: String; pVersion:Integer): Boolean;
var
  vMail: TACBrMail;
  Pasta:TFileListBox;
  I:Integer;
begin
  try
    try
      Result:=False;

      vMail:=TACBrMail.Create(Nil);
      vMail.Clear;
      vMail.From      :='commercial@greatocean.com.br';
      vMail.FromName  :='GreatOcean';
      vMail.Host      :='smtp.sendgrid.net';
      vMail.Username  :='apikey';
      vMail.Password  :='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';
      vMail.Port      :='587';
      vMail.SetTLS    :=True;
      vMail.SetSSL    :=False;
      vMail.Subject   :=pAssunto;
      vMail.IsHTML    :=True;
      vMail.UseThread :=False;
      vMail.DefaultCharset := TMailCharset(0);
      vMail.IDECharset     := TMailCharset(0);
      vMail.AddAddress(pEmail,'');
      vMail.Body.Add('<!DOCTYPE html>'+#13+
                     '<html lang="pt-BR">'+ #13+
                     '<head>'+ #13+
                     '</head> '+#13+
                     '<body>'+
                     GR.iif(pVersion=1,StringReplace(AnsiToUtf8(pHtml),'###PERSON###',pPerson,[rfReplaceAll,rfIgnoreCase]),
                     '<h4 style=''FONT-SIZE: 12pt; FONT-FAMILY: "Times New Roman", serif; WHITE-SPACE: normal; WORD-SPACING: 0px; '+
                     'TEXT-TRANSFORM: none; FONT-WEIGHT: 400; COLOR: rgb(34,34,34); FONT-STYLE: normal; ORPHANS: 2; WIDOWS: 2; MARGIN: 0cm 0cm 0pt; '+
                     'LETTER-SPACING: normal; BACKGROUND-COLOR: rgb(255,255,255); TEXT-INDENT: 0px; font-variant-ligatures: normal; '+
                     'font-variant-caps: normal; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; '+
                     'text-decoration-color: initial''> DEAR '+pPerson+ ', </h4>')+
                     //'<b style="font-weight: normal; FONT-SIZE: 12pt; FONT-FAMILY: "Times New Roman", serif; ">'+AnsiToUtf8(pHtml)+'</b>'+
                     '</body>'+ #13+
                     '</html>');

      if DirectoryExists('C:\TOTAL\BlueSparkmail\Anexos\'+pArquivo+'\') then
      begin
        Pasta:=TFileListBox.Create(Nil);
        Pasta.Visible:=False;
        Pasta.Parent:=Application.MainForm;
        Pasta.Directory:='C:\TOTAL\BlueSparkmail\Anexos\'+pArquivo+'\';

        if Pasta.Count<>0 then
        begin
          for I:=0 to Pasta.Count-1 do
          begin
            Pasta.ItemIndex:=I;
            Application.ProcessMessages;
            vMail.AddAttachment(ExtractFileName(Pasta.FileName),Pasta.FileName);
          end;
        end;

        Pasta.Directory:='';
        Pasta.FileName:='';
      end;

      vMail.Send;

      Result:=True;
    finally
      if Assigned(vMail) then
        FreeAndNil(vMail);

      if Assigned(Pasta) then
        FreeAndNil(Pasta);
    end;
  except
    on E:Exception do
    begin
      Result:=False;
    end;
  end;
end;

procedure TfPrincipal.FormCreate(Sender: TObject);
begin
  imMinimizarClick(Sender);

  lbStatus.Caption:='';
end;

procedure TfPrincipal.FormDestroy(Sender: TObject);
begin
  if vThreadListFilesFTP<>Nil then
  begin
    vThreadListFilesFTP.Suspend;
    vThreadListFilesFTP.Terminate;
  end;
  vThreadListFilesFTP:=Nil;

  if vThreadSendEmail<>Nil then
  begin
    vThreadSendEmail.Suspend;
    vThreadSendEmail.Terminate;
  end;
  vThreadSendEmail:=Nil;
end;

function TfPrincipal.FTPDirExistsTry(pDiretorio: String;
  pIdFTP: TIdFTP): Boolean;
begin
  try
    pIdFTP.ChangeDir(pDiretorio);
    Result:=True;
  except
    Result:=False;
  end;
end;

procedure TfPrincipal.imCloseClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfPrincipal.imMinimizarClick(Sender: TObject);
begin
  Self.Hide();
  Self.WindowState:=wsMinimized;
  TrayIcon.Visible:=True;
  TrayIcon.Animate:=True;
  TrayIcon.ShowBalloonHint;
end;

procedure TfPrincipal.OnTerminateThreadSendEmail(Sender: TObject);
begin
  lbStatus.Caption:='Processo Finalizado!';
  sleep(2000);
  lbThread.Tag:=0;
  vThreadSendEmail:=Nil;
  lbStatus.Caption:='';
end;

procedure TfPrincipal.Panel1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
  sc_DragMove=$f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand,sc_DragMove,0);
end;

procedure TfPrincipal.OnTerminateThreadListFilesFTP(Sender: TObject);
begin
  lbThread.Tag:=0;
  vThreadListFilesFTP:=Nil;
end;

procedure TfPrincipal.SaveLog(pMsg: String);
var
  vLog: TStringList;
  vCaminho:String;
begin
  try
    vCaminho:=ExtractFilePath(Application.ExeName)+'LogService_'+FormatDateTime('YYYYMMDD',Now)+'.ini';
    vLog:=TStringList.Create;
    try
      if FileExists(vCaminho) then
        vLog.LoadFromFile(vCaminho);

      vLog.Add(TimeToStr(Now)+': '+pMsg);
    except
      on E: Exception do
        vLog.Add(TimeToStr(Now)+': '+E.Message);
    end;
  finally
    vLog.SaveToFile(vCaminho);
    vLog.Free;
  end
end;

procedure TfPrincipal.ThreadListFilesFTP(pPasta:String);
var
  FPathFTP: string;
begin
  lbThread.Tag:=1;

  FPathFTP := 'www/DisparadorEmail/Anexos/'+pPasta+'/';
  vThreadListFilesFTP:=TThread.CreateAnonymousThread(
  procedure
  begin
      vListaArquivos:=TStringList.create;

      try
        try
          IdFTP.Disconnect;
          IdFTP.Connect;

          if IdFTP.Connected then
          begin
              if not FTPDirExistsTry(FPathFTP,IdFTP) then
              begin
                  //IdFTP.MakeDir(FPathFTP);
                  IdFTP.ChangeDir(FPathFTP);
              end;

              IdFTP.List(vListaArquivos,'',False);

              ForceDirectories('C:\TOTAL\BlueSparkmail\Anexos\'+pPasta);

              for vCountListaArquivos := 0 to Pred(vListaArquivos.Count) do
              begin
                IdFtp.Get(vListaArquivos[vCountListaArquivos],
                          'C:\TOTAL\BlueSparkmail\Anexos\'+pPasta+'\'+ExtractFileName(vListaArquivos[vCountListaArquivos]),True);
              end;
          end;
        finally
          //IdFTP.Disconnect;

          if Assigned(vListaArquivos) then
            FreeandNil(vListaArquivos);
        end;
      except
        on E: Exception do
        begin

        end;
      end;
  end);
  vThreadListFilesFTP.FreeOnTerminate:=True;
  vThreadListFilesFTP.OnTerminate:=OnTerminateThreadListFilesFTP;
  vThreadListFilesFTP.Start;
end;

procedure TfPrincipal.ThreadSendEmail;
begin
  if vThreadSendEmail<>Nil then
    Exit;

  lbStatus.Caption:='Iniciando Processo...';

  lbThread.Tag:=1;

  vThreadSendEmail:=TThread.CreateAnonymousThread(
  procedure
  var
    vFDQuery,vFDQuery2,vFDQuery3:TFDQuery;
    vDataAtual,vData,vTime:TDateTime;
    vList:TStringList;
    vDestino:String;
    I:integer;
    vNome: string;
  begin
      try
          vDataAtual:=Now;
          vData:=DateOf(vDataAtual);
          vTime:=TimeOf(vDataAtual);

          vList:=TStringList.Create;

          TThread.Synchronize(vThreadSendEmail,
            procedure
            begin
              lbStatus.Caption:='Selecionando Emails n�o enviados';
            end);

          vFDQuery:=TFDQuery.Create(Nil);
          vFDQuery.Connection:=FDConGreatOcean;

          vFDQuery2:=TFDQuery.Create(Nil);
          vFDQuery2.Connection:=FDConGreatOcean;

          vFDQuery3:=TFDQuery.Create(Nil);
          vFDQuery3.Connection:=FDConGreatOcean;

          try
              vFDQuery.SQL.Clear;
              vFDQuery.SQL.Add('SELECT ID_EMAIL,EMAIL_ASSUNTO,EMAIL_BODY,DATA_ENVIADO,HORA_ENVIADO,ENVIADO,VERSION FROM EMAILS EM');
              //vFDQuery.SQL.Add('WHERE ID_EMAIL=108');
              vFDQuery.SQL.Add('WHERE ENVIADO = 0 AND DATA_AGENDAMENTO=:D1 AND HORA_AGENDAMENTO<=:H1');
              vFDQuery.ParamByName('D1').AsDateTime := vData;
              vFDQuery.ParamByName('H1').AsTime     := vTime;
              vFDQuery.Open();

              FDConGreatOcean.StartTransaction;

              while not vFDQuery.Eof do
              begin
                  TThread.Synchronize(vThreadSendEmail,
                    procedure
                    begin
                      lbStatus.Caption:='Downloads dos Anexos';
                    end);


                  ThreadListFilesFTP(vFDQuery.FieldByName('ID_EMAIL').AsString);

                  while lbThread.Tag=1 do
                    Application.ProcessMessages;


                  TThread.Synchronize(vThreadSendEmail,
                    procedure
                    begin
                      lbStatus.Caption:='Enviando Emails'
                    end);

                  vFDQuery2.SQL.Clear;
                  vFDQuery2.SQL.Add('SELECT EC.ID_EMAIL_CLIENTE,CLI.ID_CLIENTE,CLI.PERSON,CLI.EMAIL FROM EMAILS_CLIENTES EC');
                  vFDQuery2.SQL.Add('INNER JOIN CLIENTES CLI ON CLI.ID_CLIENTE=EC.ID_CLIENTE');
                  vFDQuery2.SQL.Add('WHERE EC.ID_EMAIL='+vFDQuery.FieldByName('ID_EMAIL').AsString);
                  vFDQuery2.Open();
                  vFDQuery2.First;

                  while not vFDQuery2.Eof do
                  begin
                      if EnviarEmailAcbr(
                           vFDQuery.FieldByName('EMAIL_BODY').AsString,
                           vFDQuery2.FieldByName('EMAIL').AsString,
                           vFDQuery.FieldByName('EMAIL_ASSUNTO').AsString,
                           vFDQuery.FieldByName('ID_EMAIL').AsString,
                           vFDQuery2.FieldByName('PERSON').AsString,
                           vFDQuery.FieldByName('VERSION').AsInteger) then
                      begin
                          vFDQuery3.SQL.Clear;
                          vFDQuery3.SQL.Add('UPDATE EMAILS_CLIENTES SET ENVIADO=1 WHERE ID_EMAIL_CLIENTE='+vFDQuery2.FieldByName('ID_EMAIL_CLIENTE').AsString);
                          vFDQuery3.ExecSQL;
                      end;

                      vFDQuery2.Next;
                  end;

                  vFDQuery.Edit;
                  vFDQuery.FieldByName('ENVIADO').AsInteger:=1;
                  vFDQuery.FieldByName('DATA_ENVIADO').AsDateTime:=vData;
                  vFDQuery.FieldByName('HORA_ENVIADO').AsDateTime:=vTime;
                  vFDQuery.Post;

                  vFDQuery.Next;
              end;

              FDConGreatOcean.Commit;

          finally
              if Assigned(vFDQuery) then
                FreeandNil(vFDQuery);

              if Assigned(vFDQuery2) then
                FreeandNil(vFDQuery2);

              if Assigned(vFDQuery3) then
                FreeandNil(vFDQuery3);

              if Assigned(vList) then
                FreeandNil(vList);
          end;
      except
        on E: Exception do
        begin
            FDConGreatOcean.Rollback;
            SaveLog(E.Message);
        end;
      end;
  end);
  vThreadSendEmail.FreeOnTerminate:=True;
  vThreadSendEmail.OnTerminate:=OnTerminateThreadSendEmail;
  vThreadSendEmail.Start;
end;

procedure TfPrincipal.TrayIconClick(Sender: TObject);
begin
  TrayIcon.Visible:=False;
  Show();
  WindowState:=wsNormal;
  Application.BringToFront();
end;

procedure TfPrincipal.TVerificarEmailTimer(Sender: TObject);
begin
  ThreadSendEmail;
end;

end.
