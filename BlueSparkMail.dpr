program BlueSparkMail;

uses
  Vcl.Forms,
  BlueSpark in 'BlueSpark.pas' {fPrincipal},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Metropolis UI Dark');
  Application.CreateForm(TfPrincipal, fPrincipal);
  Application.Run;
end.
